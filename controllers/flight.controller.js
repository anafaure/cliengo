const axios = require('axios');

exports.filter = async (req, res) => {
	const launches = await axios.get(process.env.LAUNCHES);
    const rocket = await axios.get(process.env.ROCKETS);
    const infoLaunches = launches.data.find(elem => elem.flight_number === 39);
    const infoRocket = rocket.data.find(elem => elem.rocket_id === 'falcon9');

    const images = [
       infoRocket.flickr_images[0],
       infoRocket.flickr_images[1],
       infoRocket.flickr_images[2]
    ] 
    const rocketObj = {
        rocket_id: infoRocket.rocket_id,
        rocket_name: infoRocket.rocket_name,
        description: infoRocket.description,
        images 
    }
    const payloads = {
        payload_id : infoLaunches.rocket.second_stage.payloads[0].payload_id,
        manufacture: infoLaunches.rocket.second_stage.payloads[0].manufacturer,
        type: infoLaunches.rocket.second_stage.payloads[0].payload_type
    }
    const result = [{
        flight_number : infoLaunches.flight_number,
        mission_name : infoLaunches.mission_name,
        rocketObj,
        payloads 
    }];
    return res.json(result);
}