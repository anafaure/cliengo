const express = require("express");
const app = express();
require('dotenv').config();

var flight=require('./routes/flight.route.js');

app.use('/flight', flight);

app.listen(process.env.PORT, () => {
 console.log("El servidor está inicializado en el puerto: ", process.env.PORT);
});